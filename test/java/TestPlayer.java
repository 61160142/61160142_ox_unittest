/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import mywork.Player;

/**
 *
 * @author Yumat
 */
public class TestPlayer {
    
    public TestPlayer() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    
    public void testShowTurn(){
        Player p = new Player("X");
        p.showTurn();
    }
    public void testChangePlayer(){
        Player p = new Player("X");
        p.changeName("O");
        p.showTurn();
    }
}
