/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import mywork.Board;
import mywork.Player;

/**
 *
 * @author Yumat
 */
public class TestBoard {
    
    public TestBoard() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    public void testNewBoard(){ 
        String[][] board = {{"-","-","-"},{"-","-","-"},{"-","-","-"}};
    }
    public void testShowBoard(){
        Board board = new Board();
        board.showBoard();    
    }
    public void testCheckInput(){
        Board b = new Board();
        Player player = new Player("X");
        assertEquals(true,b.checkInput(player));

    }
    public void testCheckXWinVertical(){
        String[][] board = {{"X","-","-"},{"X","-","-"},{"X","-","-"}};
        Board b = new Board();
        b.newBoard(board);
        Player player = new Player("X");
        b.showBoard();
        player.showTurn();
        int round = 0;
        if(b.checkInput(player)==true) {
                if(player.getName()=="X")
                        player.changeName("O");
                else
                        player.changeName("X");
                round++;
        }
        b.showBoard();
        assertEquals(false,b.checkWin(round));
    }
    public void testCheckOWinVertical(){
        String[][] board = {{"O","-","X"},{"O","X","-"},{"O","-","-"}};
        Board b = new Board();
        b.newBoard(board);
        Player player = new Player("X");
        b.showBoard();
        player.showTurn();
        int round = 0;
        if(b.checkInput(player)==true) {
                if(player.getName()=="X")
                        player.changeName("O");
                else
                        player.changeName("X");
                round++;
        }
        b.showBoard();
        assertEquals(false,b.checkWin(round));
    }
    
    public void testCheckXWinHorizon(){
        String[][] board = {{"X","X","X"},{"O","O","-"},{"-","-","-"}};
        Board b = new Board();
        b.newBoard(board);
        Player player = new Player("X");
        b.showBoard();
        player.showTurn();
        int round = 0;
        if(b.checkInput(player)==true) {
                if(player.getName()=="X")
                        player.changeName("O");
                else
                        player.changeName("X");
                round++;
        }
        b.showBoard();
        assertEquals(false,b.checkWin(round));
    }
    public void testCheckOWinHorizon(){
        String[][] board = {{"O","O","O"},{"X","X","-"},{"-","-","-"}};
        Board b = new Board();
        b.newBoard(board);
        Player player = new Player("X");
        b.showBoard();
        player.showTurn();
        int round = 0;
        if(b.checkInput(player)==true) {
                if(player.getName()=="X")
                        player.changeName("O");
                else
                        player.changeName("X");
                round++;
        }
        b.showBoard();
        assertEquals(false,b.checkWin(round));
    }
    public void testCheckXWinDiagonal(){
        String[][] board = {{"X","O","O"},{"O","X","-"},{"-","-","X"}};
        Board b = new Board();
        b.newBoard(board);
        Player player = new Player("X");
        b.showBoard();
        player.showTurn();
        int round = 0;
        if(b.checkInput(player)==true) {
                if(player.getName()=="X")
                        player.changeName("O");
                else
                        player.changeName("X");
                round++;
        }
        b.showBoard();
        assertEquals(false,b.checkWin(round));
    }
    public void testCheckOWinDiagonal(){
        String[][] board = {{"O","X","X"},{"X","O","-"},{"-","-","O"}};
        Board b = new Board();
        b.newBoard(board);
        Player player = new Player("X");
        b.showBoard();
        player.showTurn();
        int round = 0;
        if(b.checkInput(player)==true) {
                if(player.getName()=="X")
                        player.changeName("O");
                else
                        player.changeName("X");
                round++;
        }
        b.showBoard();
        assertEquals(false,b.checkWin(round));
    }
}
