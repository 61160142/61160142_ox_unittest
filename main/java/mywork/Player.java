package mywork;

public class Player {
	String name = "Unknow";
	int wincount = 0;
	
	public Player(String name){ 
		this.name = name;
	}
	public Player(){
		name = "Unknow";
		wincount = 0;
	}
	public void showTurn() {
		System.out.println(name+"'s Turn");
	}
	public void changeName(String n) {
		name = n;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getWinCount() {
		return wincount;
	}
	public void setWinCount(int wincount) {
		this.wincount = wincount;
	}
	public void addWincount() {
		wincount++;
	}
	
}
