
package mywork;
import java.util.Scanner;

public class Game {
        Scanner kb = new Scanner(System.in);
        Player player = new Player("X");
        String again="Y";
        Board b = new Board();
    public void run(){
        showWelcome();
        while(again.equals("Y")) {
                int round = 0;
                b.newBoard();
                while(round<9) {
                        b.showBoard();
                        player.showTurn();
                        b.inputNumber();
                        if(b.checkInput(player)==true) {
                                if(player.getName()=="X")
                                        player.changeName("O");
                                else
                                        player.changeName("X");
                                round++;
                        }
                        if(b.checkWin(round)==false) {
                                break;
                        }
                }
                showAgain();
                again = kb.next();
        }
        b.showScore();

    }
    public void showWelcome() {
        System.out.println("=====  Welcome to XO GAME  ===== ");
    }
    
    public static void showAgain() {
            System.out.println("Do you want to play again? (N/Y)");
    }
}
