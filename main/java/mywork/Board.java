package mywork;

import java.util.Scanner;

public class Board{
	String[][] board = {{"-","-","-"},{"-","-","-"},{"-","-","-"}};
	int row,col;
	Player px = new Player("X");
	Player po = new Player("O");
	boolean win = true;
	
	public void showBoard() {
		System.out.println("+---+---+---+");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print("| ");
				System.out.print(board[i][j]);
				System.out.print(" ");
			}
			System.out.print("| ");
			System.out.println();
			System.out.println("+---+---+---+");
		}
	}
	public void inputNumber() {
                System.out.print("Please input Row and Column : ");
		Scanner kb = new Scanner(System.in);
		row = kb.nextInt()-1;
		col = kb.nextInt()-1;
		System.out.println();
	}
	public boolean checkInput(Player p) {
		if (row > 2 || row < 0) {
			System.out.println("====Number of Row error!! Please input only 1, 2 or 3====\n");
			return false;
		} else if (col > 2 || col < 0) {
			System.out.println("====Number of Column error!! Please input only 1, 2 or 3====\n");
			return false;
		} else if (board[row][col] == "O" || board[row][col] == "X") {
			System.out.println("====This place is already taken!! Please try again ====\n");
			return false;
		} else {
			board[row][col] = p.getName();
			return true;
		}
		
	}
	public void newBoard() {
		board = new String[3][3];
		for(int i=0;i<3;i++) {
			for(int j =0;j<3;j++) {
				board[i][j] = "-";
			}
		}
		win = true;
	}
        public void newBoard(String[][] b) {
		board = new String[3][3];
		for(int i=0;i<3;i++) {
			for(int j =0;j<3;j++) {
				board[i][j] = b[i][j];
			}
		}
		win = true;
	}
        
	public void showScore() {
		System.out.println("=====  Score  ===== ");
		System.out.println("O's score = Win : "+po.getWinCount());
		System.out.println("X's score = Win : "+px.getWinCount());
		System.out.println("\nThank you for playing \"XO Game\"");
	}
	public boolean checkWin(int round) {
		checkXWin();
		checkOWin();
		checkDraw(round);
		return win;
	}
	public void checkXWin(){
		checkXWinHorizon();
		checkXWinVertical();
		checkXWinDiagonal();

	}
	public void checkXWinHorizon() {
		if(board[0][0]=="X" && board[0][1]=="X" && board[0][2]=="X") {
			showWinnerX();
		}
		else if (board[1][0]=="X" && board[1][1]=="X" && board[1][2]=="X"){
			showWinnerX();
		}
		else if (board[2][0]=="X" && board[2][1]=="X" && board[2][2]=="X"){
			showWinnerX();
		}
		
	}
	public void checkXWinVertical() {
		if(board[0][0]=="X" && board[1][0]=="X" && board[2][0]=="X"){
			showWinnerX();
		}
		else if(board[0][1]=="X" && board[1][1]=="X" && board[2][1]=="X"){
			showWinnerX();
		}
		else if(board[0][2]=="X" && board[1][2]=="X" && board[2][2]=="X"){
			showWinnerX();
		}
	}
	public void checkXWinDiagonal() {
		if(board[0][0]=="X" && board[1][1]=="X" && board[2][2]=="X"){
			showWinnerX();
		}
		else if(board[0][2]=="X" && board[1][1]=="X" && board[2][0]=="X"){
			showWinnerX();
		}
	}
	public void showWinnerX() {
		px.addWincount();;
		win = false;
		System.out.println("\n===== X is the winner!! =====\n");
		showBoard();
	}
	public void checkOWin() {			
		checkOWinHorizon();
		checkOWinVertical();
		checkOWinDiagonal();
	}
	public void checkOWinHorizon() {
		if(board[0][0]=="O" && board[0][1]=="O" && board[0][2]=="O")
			showWinnerO();
		else if (board[1][0]=="O" && board[1][1]=="O" && board[1][2]=="O")
			showWinnerO();
		else if (board[2][0]=="O" && board[2][1]=="O" && board[2][2]=="O")
			showWinnerO();
	}
	public void checkOWinVertical() {
		if(board[0][0]=="O" && board[1][0]=="O" && board[2][0]=="O")
			showWinnerO();
		else if(board[0][1]=="O" && board[1][1]=="O" && board[2][1]=="O")
			showWinnerO();
		else if(board[0][2]=="O" && board[1][2]=="O" && board[2][2]=="O")
			showWinnerO();
	}
	public void checkOWinDiagonal() {
		if(board[0][0]=="O" && board[1][1]=="O" && board[2][2]=="O")
			showWinnerO();
		else if(board[0][2]=="O" && board[1][1]=="O" && board[2][0]=="O")
			showWinnerO();
	}

	public void showWinnerO() {
		po.addWincount();
		win = false;
		System.out.println("\n===== O is the winner!! =====\n");
		showBoard();
	}
	public void checkDraw(int round) {
		checkODraw(round);
		checkXDraw(round);
	}
	public void checkODraw(int round) {
		int r =0;
		int c =0;
		if(round==9 && !((board[r][c] == "O" && board[r][c+1] == "O" && board[r][c+2] == "O") ||
		  (board[r][c] == "O" && board[r+1][c] == "O" && board[r+2][c] == "O") || 
		  (board[r][c] == "O" && board[r+1][c+1] == "O" && board[r+2][c + 2] == "O") || 
		  (board[r+1][c] == "O" && board[r+1][c+1] == "O" && board[r+1][c+2] == "O") || 
		  (board[r+2][c] == "O" && board[r+2][c+1] == "O" && board[r+2][c+2] == "O") || 
		  (board[r][c+1] == "O" && board[r+1][c+1] == "O" && board[r+2][c+1] == "O") || 
		  (board[r][c+2] == "O" && board[r+1][c+2] == "O" && board[r+2][c+2] == "O") || 
		  (board[r][c+2] == "O" && board[r+1][c+1] == "O"&& board[r+2][c] == "O"))) {
		}
	}
	public void checkXDraw(int round) {
		int r =0;
		int c =0;
		if(round==9 && !((board[r][c] == "X" && board[r][c+1] == "X" && board[r][c+2] == "X")|| 
			(board[r][c] == "X" && board[r+1][c] == "X" && board[r+2][c] == "X") || 
			(board[r][c] == "X" && board[r+1][c+1] == "X" && board[r+2][c + 2] == "X") || 
			(board[r+1][c] == "X" && board[r+1][c+1] == "X" && board[r+1][c+2] == "X") || 
			(board[r+2][c] == "X" && board[r+2][c+1] == "X" && board[r+2][c+2] == "X") || 
			(board[r][c+1] == "X" && board[r+1][c+1] == "X" && board[r+2][c+1] == "X") || 
			(board[r][c+2] == "X" && board[r+1][c+2] == "X" && board[r+2][c+2] == "X") || 
			(board[r][c+2] == "X" && board[r+1][c+1] == "X"&& board[r+2][c] == "X"))) {
			showDraw();
		}
	}
	private void showDraw() {
		win = false;
		System.out.println("\n===== Draw =====\n");
		showBoard();		
	}

}
